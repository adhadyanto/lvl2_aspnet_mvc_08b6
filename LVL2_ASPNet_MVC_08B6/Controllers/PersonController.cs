﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_08B6.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        public ActionResult Index()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("FirstName", "Ken");
            report.ServerReport.ReportPath = "/PersonReport";
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}